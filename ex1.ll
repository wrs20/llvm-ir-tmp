; ModuleID = "test"
target triple = "unknown-unknown-unknown"
target datalayout = ""

declare <8 x double> @"llvm.fma.v8f64"(<8 x double> %".1", <8 x double> %".2", <8 x double> %".3") 

define i32 @"dgemm_block_inner"(<8 x double>* %".1", <8 x double>* %".2", <8 x double>* %".3") 
{
entry:
  %".11" = load <8 x double>, <8 x double>* %".1"
  %".12" = load <8 x double>, <8 x double>* %".2"
  %".13" = load <8 x double>, <8 x double>* %".3"
  %".o1" = call <8 x double> @"llvm.fma.v8f64"(<8 x double> %".11", <8 x double> %".12", <8 x double> %".13")
  store <8 x double> %".o1", <8 x double>* %".3"
  ret i32 0
}
