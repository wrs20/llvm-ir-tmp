	.text
	.file	"ex1.ll"
	.globl	dgemm_block_inner       # -- Begin function dgemm_block_inner
	.p2align	4, 0x90
	.type	dgemm_block_inner,@function
dgemm_block_inner:                      # @dgemm_block_inner
	.cfi_startproc
# %bb.0:                                # %entry
	vmovapd	(%rdi), %zmm0
	vmovapd	(%rsi), %zmm1
	vfmadd213pd	(%rdx), %zmm0, %zmm1
	vmovapd	%zmm1, (%rdx)
	xorl	%eax, %eax
	vzeroupper
	retq
.Lfunc_end0:
	.size	dgemm_block_inner, .Lfunc_end0-dgemm_block_inner
	.cfi_endproc
                                        # -- End function

	.section	".note.GNU-stack","",@progbits
